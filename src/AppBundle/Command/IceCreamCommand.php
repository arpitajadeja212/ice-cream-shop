<?php

namespace AppBundle\Command;

use AppBundle\Event\CalculateDiscountEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Represents a ice-cream shop.
 *
 * @author Arpita Rana
 */
class IceCreamCommand extends ContainerAwareCommand
{
    private $numberOfScoops;
    private $selectedToppings;
    private $scoopManager;
    private $toppingManager;

    protected function configure()
    {
        $this
            ->setName('app:create-ice-cream')
            ->setDescription('Creates ice cream.')
            ->setHelp('This command allows you to create a ice cream...');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->scoopManager = $this->getContainer()->get('app.scoop.manager');
        $this->toppingManager = $this->getContainer()->get('app.topping.manager');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new Question('Please enter your number of scoops : ');
        $question->setValidator(function ($answer) {
            if ($answer < 1) {
                throw new \RuntimeException(
                    'Pleae enter number of scoop greater than 0'
                );
            }
            return $answer;
        });
        $this->numberOfScoops = $helper->ask($input, $output, $question);

        $toppings = $this->toppingManager->getAllToppingDetail();
        $question = new ChoiceQuestion(
            'Please select your favorite toppings',
            $toppings
        );
        $question->setMultiselect(true);
        $this->selectedToppings = $helper->ask($input, $output, $question);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $scoopAmount = $this->scoopManager->getTotalAmountOfScoop($this->numberOfScoops);
        $toppingAmount = $this->toppingManager->getTotalAmountOfTopping($this->selectedToppings);

        $totalAmount = $this->getContainer()->get('app.math.operation.manager')->getSumOfValue([$scoopAmount, $toppingAmount]);

        $event = new CalculateDiscountEvent($totalAmount);
        $dispatcher = $this->getContainer()->get('event_dispatcher');
        $dispatcher->dispatch('app.event.discount', $event);
        $eventAmount = $event->getAmount();

        $output->writeln('Your total billed amount : '.$eventAmount);
    }
}