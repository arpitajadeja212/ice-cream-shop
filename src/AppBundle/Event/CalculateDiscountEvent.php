<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class for calculate discount on calling of event.
 *
 * @author Arpita Rana
 */
class CalculateDiscountEvent extends Event
{
    const ICE_CREAM_DISCOUNT = 10;
    private $totalAmount;

    /**
     * @param integer $totalAmount Total amount
     */
    public function __construct($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    public function calculateDiscountOnAmount()
    {
        if($this->totalAmount > 100) {
            $this->totalAmount = ($this->totalAmount - (($this->totalAmount * self::ICE_CREAM_DISCOUNT ) / 100));
        }
    }

    /**
     * Returns total amount.
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->totalAmount;
    }
}