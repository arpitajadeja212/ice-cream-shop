<?php

namespace AppBundle\Services;

/**
 * Class for defined scoop price.
 *
 * @author Arpita Rana
 */
class ScoopManager
{
    const SCOOP_PRICE_PER_UNIT = 10;

    /**
     * Returns total price by unit of scoop.
     *
     * @param integer $numberOfScoop number of scoop.
     *
     * @return integer
     */
    public function getTotalAmountOfScoop($numberOfScoop)
    {
        return ($numberOfScoop * self::SCOOP_PRICE_PER_UNIT);
    }
}