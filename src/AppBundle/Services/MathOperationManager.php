<?php

namespace AppBundle\Services;

/**
 * Class for call the events from command.
 *
 * @author Arpita Rana
 */
class MathOperationManager
{
    /**
     * Returns total sum of amount.
     *
     * @param array $amount array of amount.
     *
     * @return integer
     */
    public function getSumOfValue($amount)
    {
        return array_sum($amount);
    }
}