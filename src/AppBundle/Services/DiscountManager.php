<?php

namespace AppBundle\Services;

use AppBundle\Event\CalculateDiscountEvent;

/**
 * Class for call the events from command.
 *
 * @author Arpita Rana
 */
class DiscountManager
{
    public function onCalculateDiscountEvent(CalculateDiscountEvent $calculateDiscountEvent)
    {
        $calculateDiscountEvent->calculateDiscountOnAmount();
    }
}