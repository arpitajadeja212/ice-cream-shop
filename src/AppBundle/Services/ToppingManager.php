<?php

namespace AppBundle\Services;

/**
 * Class for defined topping lists and price.
 *
 * @author Arpita Rana
 */
class ToppingManager
{
    const SCOOP_PRICE_PER_UNIT = [
        'oreo' => 50,
        'cream' => 100,
        'chocalate' => 200,
        'rasbeery' => 250
    ];

    /**
     * Returns total amount of toppings from the constant lists.
     *
     * @param array $toppings name of toppings.
     *
     * @return integer
     */
    public function getTotalAmountOfTopping($toppings)
    {
        $toppingAmount = 0;
        foreach($toppings as $index => $topping) {
            $toppingAmount += $this->getToppingPrice($topping);
        }

        return $toppingAmount;
    }

    /**
     * Returns price of given toping name.
     *
     * @param string $toppingName name of topping.
     *
     * @return integer
     */
    public function getToppingPrice($toppingName)
    {
        return self::SCOOP_PRICE_PER_UNIT[$toppingName];
    }

    /**
     * Returns all lists of topping with name and price.
     *
     * @return array
     */
    public function getAllToppingDetail()
    {
        return self::SCOOP_PRICE_PER_UNIT;
    }
}